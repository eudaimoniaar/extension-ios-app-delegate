#import <OpenFlAppDelegate.h>
#import <Foundation/Foundation.h>

@implementation OpenFlAppDelegate

+ (id)sharedInstance {
    static dispatch_once_t onceQueue;
    static OpenFlAppDelegate* instance = nil;
    
    dispatch_once(&onceQueue, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (id) init {
    if (self = [super init]) {
    	NSLog(@"Init OpenFlAppDelegate");
        self.delegates = [[NSMutableArray alloc] init];
    }
    return self;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
	bool result = YES;

	NSLog(@"application openURL... calling delegates...");

	for(id delegate in self.delegates) {
 		if ([delegate respondsToSelector:@selector(application:openURL:sourceApplication:annotation:)]) {
	 		result = result && [delegate application:application
											openURL:url
											sourceApplication:sourceApplication
											annotation:annotation];
	 	}
	}

	return result;
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	NSLog(@"application didReceiveRemoteNotification... calling delegates...");

	for(id delegate in self.delegates) {
		if ([delegate respondsToSelector:@selector(application:didReceiveRemoteNotification:)]) {
			[delegate application:application
						didReceiveRemoteNotification:userInfo];
		}
	}
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
	NSLog(@"application didRegisterForRemoteNotificationsWithDeviceToken... calling delegates...");

	for(id delegate in self.delegates) {
		if ([delegate respondsToSelector:@selector(application:didRegisterForRemoteNotificationsWithDeviceToken:)]) {
			[delegate application:application
						didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
		}
	}
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
	NSLog(@"application didFailToRegisterForRemoteNotificationsWithError... calling delegates...");

	for(id delegate in self.delegates) {
		if ([delegate respondsToSelector:@selector(application:didFailToRegisterForRemoteNotificationsWithError:)]) {
			[delegate application:application
						didFailToRegisterForRemoteNotificationsWithError:error];
		}
	}
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions :(NSDictionary *)launchOptions {
	bool result = YES;

	NSLog(@"application willFinishLaunchingWithOptions... calling delegates...");

	for(id delegate in self.delegates) {
 		if ([delegate respondsToSelector:@selector(application:willFinishLaunchingWithOptions:)]) {
	 		result = result && [delegate application:application
											willFinishLaunchingWithOptions:launchOptions];
	 	}
	}

	return result;
}

- (void)addAppDelegate:(id)delegate {
	NSLog(@"Adding app delegate");
	[self.delegates addObject: delegate];
}

@end
