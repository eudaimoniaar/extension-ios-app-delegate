#import <NMEAppDelegate.h>
#import <OpenFlAppDelegate.h>
#import <UIKit/UIKit.h>


@implementation NMEAppDelegate (ExtensionIOSAppDelegate)

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
	
	return [[OpenFlAppDelegate sharedInstance] application:application
												openURL:url
												sourceApplication:sourceApplication
												annotation:annotation];
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	
	[[OpenFlAppDelegate sharedInstance] application:application
										didReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
	
	[[OpenFlAppDelegate sharedInstance] application:application
										didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {

	[[OpenFlAppDelegate sharedInstance] application:application
										didFailToRegisterForRemoteNotificationsWithError:error];
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions :(NSDictionary *)launchOptions {
	
	return [[OpenFlAppDelegate sharedInstance] application:application
												willFinishLaunchingWithOptions:launchOptions];
}

@end
